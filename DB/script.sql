USE [master]
GO
/****** Object:  Database [labtest_db]    Script Date: 16-Feb-20 11:24:09 PM ******/
CREATE DATABASE [labtest_db]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'labtest_db', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQL2019\MSSQL\DATA\labtest_db.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'labtest_db_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQL2019\MSSQL\DATA\labtest_db_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [labtest_db] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [labtest_db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [labtest_db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [labtest_db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [labtest_db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [labtest_db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [labtest_db] SET ARITHABORT OFF 
GO
ALTER DATABASE [labtest_db] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [labtest_db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [labtest_db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [labtest_db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [labtest_db] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [labtest_db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [labtest_db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [labtest_db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [labtest_db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [labtest_db] SET  ENABLE_BROKER 
GO
ALTER DATABASE [labtest_db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [labtest_db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [labtest_db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [labtest_db] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [labtest_db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [labtest_db] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [labtest_db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [labtest_db] SET RECOVERY FULL 
GO
ALTER DATABASE [labtest_db] SET  MULTI_USER 
GO
ALTER DATABASE [labtest_db] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [labtest_db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [labtest_db] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [labtest_db] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [labtest_db] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'labtest_db', N'ON'
GO
ALTER DATABASE [labtest_db] SET QUERY_STORE = OFF
GO
USE [labtest_db]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 16-Feb-20 11:24:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Syllabus]    Script Date: 16-Feb-20 11:24:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Syllabus](
	[SyllabusId] [int] IDENTITY(1,1) NOT NULL,
	[LevelId] [int] NOT NULL,
	[SyllabusName] [nvarchar](max) NOT NULL,
	[Language] [nvarchar](max) NOT NULL,
	[SyllabusFile] [nvarchar](max) NULL,
	[TestplanFile] [nvarchar](max) NULL,
	[DevOfficer] [nvarchar](max) NOT NULL,
	[Manager] [nvarchar](max) NOT NULL,
	[ActiveDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Syllabus] PRIMARY KEY CLUSTERED 
(
	[SyllabusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Trade]    Script Date: 16-Feb-20 11:24:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Trade](
	[TradeId] [int] IDENTITY(1,1) NOT NULL,
	[TradeName] [nvarchar](max) NULL,
 CONSTRAINT [PK_Trade] PRIMARY KEY CLUSTERED 
(
	[TradeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TradeLevel]    Script Date: 16-Feb-20 11:24:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TradeLevel](
	[TradeLevelId] [int] IDENTITY(1,1) NOT NULL,
	[TradeId] [int] NOT NULL,
	[Level] [nvarchar](max) NULL,
 CONSTRAINT [PK_TradeLevel] PRIMARY KEY CLUSTERED 
(
	[TradeLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GetSyllabus]    Script Date: 16-Feb-20 11:24:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSyllabus]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT Syllabus.SyllabusId, Syllabus.LevelId, Syllabus.SyllabusName, Syllabus.Language,
  Syllabus.SyllabusFile, Syllabus.TestplanFile, Syllabus.DevOfficer, Syllabus.Manager, Syllabus.ActiveDate, Trade.TradeName, TradeLevel.[Level],
 
       RIGHT(Syllabus.SyllabusFile, CHARINDEX('\', REVERSE(Syllabus.SyllabusFile)) -1)  [sylfile_name],

	  
       RIGHT(TestplanFile, CHARINDEX('\', REVERSE(TestplanFile)) -1)  [testfile_name]
FROM     TradeLevel INNER JOIN
                  Trade ON TradeLevel.TradeId = Trade.TradeId INNER JOIN
                  Syllabus ON TradeLevel.TradeLevelId = Syllabus.LevelId


END
GO
USE [master]
GO
ALTER DATABASE [labtest_db] SET  READ_WRITE 
GO
