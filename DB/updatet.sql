USE [labtest_db]
GO
/****** Object:  StoredProcedure [dbo].[GetSyllabus]    Script Date: 26-Feb-20 11:42:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSyllabus]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT Syllabus.SyllabusId, Syllabus.LevelId, Syllabus.SyllabusName, Syllabus.Language,
  Syllabus.SyllabusFile, Syllabus.TestplanFile, Syllabus.DevOfficer, Syllabus.Manager, Syllabus.ActiveDate, Trade.TradeName, TradeLevel.[Level],TradeLevel.TradeId,
 
       RIGHT(Syllabus.SyllabusFile, CHARINDEX('\', REVERSE(Syllabus.SyllabusFile)) -1)  [sylfile_name],

	  
       RIGHT(TestplanFile, CHARINDEX('\', REVERSE(TestplanFile)) -1)  [testfile_name]
FROM     TradeLevel INNER JOIN
                  Trade ON TradeLevel.TradeId = Trade.TradeId INNER JOIN
                  Syllabus ON TradeLevel.TradeLevelId = Syllabus.LevelId


END
GO
