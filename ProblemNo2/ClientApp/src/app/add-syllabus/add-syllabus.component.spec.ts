﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { AddSyllabusComponent } from './add-syllabus.component';

let component: AddSyllabusComponent;
let fixture: ComponentFixture<AddSyllabusComponent>;

describe('addSyllabus component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AddSyllabusComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(AddSyllabusComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});