import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm, FormGroup, FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-add-syllabus',
    templateUrl: './add-syllabus.component.html',
    styleUrls: ['./add-syllabus.component.css']
})
/** addSyllabus component*/
export class AddSyllabusComponent implements  OnInit  {
/** addSyllabus ctor */

  public trades: any[];
  public levels: any[];
  public http: HttpClient;
  public baseUrl: string;
  Language: string;
  file1: File;
  file2: File;

  checkedList = [];
  xyzlist = [
  {
    id: 'En',
    value: 'English'
  },
  {
    id: 'Bn',
    value: 'Bangla'
    },
    {
      id: 'Kr',
      value: 'Korean'
    }
];
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.baseUrl = baseUrl;
    this.getTrade();
  }
  ngOnInit() {
    //this.uploadForm = this.formBuilder.group({
    //  SyllabusFile: ['']
    //});

    if (true) {

    }
  }

  
  getTrade() {
    this.http.get<any[]>(this.baseUrl + 'api/Syllabus/GetTrade').subscribe(result => {
      this.trades = result;
    }, error => console.error(error));
  }

  dataChanged(newObj) {
    this.http.get<any[]>(this.baseUrl + 'api/Syllabus/GetLevel/' + newObj).subscribe(result => {
      this.levels = result;
    }, error => console.error(error));
  }


  onSubmit(f: NgForm) {
    console.log(f.value);  // { first: '', last: '' }
    console.log(f.valid);  // false
    if (f.valid) {
      if (this.checkedList.length>0) {
        f.controls['Language'].setValue(this.checkedList.toString())
      } else {
        alert('At Least one Language required');
        return;
      }

      if (this.file1 == null) {
        alert('Syllabus File is required');
        return;
      }
      if (this.file2 == null) {
        alert('Test Plan File is required');
        return;
      }
      const formData = new FormData();
      formData.append('SyllabusFile', this.file1);
      formData.append('TestPlanFile', this.file2);
      formData.append('data', JSON.stringify(f.value))

      this.http.post<any>(this.baseUrl + 'api/Syllabus/SaveSyllabus/', formData)
        .subscribe(
          (res) => alert('Save Successfully.'),
          (err) => console.log(err)
        );
    }
    else {
      alert('Form is invaild');
    }

   
  }
  onCheckboxChange(option, event) {
    if (event.target.checked) {
      this.checkedList.push(option.id);
    } else {
      for (var i = 0; i < this.xyzlist.length; i++) {
        if (this.checkedList[i] == option.id) {
          this.checkedList.splice(i, 1);
        }
      }
    }
    this.Language = this.checkedList.toString();
    console.log(this.checkedList.toString());
  }
  onFileSelectSyllabus(event) {
    if (event.target.files.length > 0) {
       this.file1 = event.target.files[0];
      
    }
  }
  onFileSelectTestPlan(event) {
    if (event.target.files.length > 0) {
      this.file2 = event.target.files[0];
    }
  }
}

