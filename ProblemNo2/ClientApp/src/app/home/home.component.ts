import { Component, Inject, ViewChild, ElementRef, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { DataTablesModule } from 'angular-datatables';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit{
 
  public http: HttpClient;
  public baseUrl: string;
  public syllabuses: any[];
  dataTable: any;

  tableData = [];
  @ViewChild('myDataTable', { static: true }) table;
  dtOptions: DataTables.Settings = {};

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.baseUrl = baseUrl;
    
  }
  ngOnInit() {
    this.getSyllabus();
    this.dtOptions = {      
      columns: [],
      order: []
    };
  }
  getSyllabus() {
    this.http.get<any[]>(this.baseUrl + 'api/Syllabus/GetSyllabus').subscribe(result => {
      this.syllabuses = result;

      //this.tableData = this.syllabuses;
      //this.dtOptions = {
      //  data: this.tableData
      //}

      console.log(this.dtOptions);
        
    }, error => console.error(error));
  }


  download(id:any) {
    this.http.get(this.baseUrl + 'api/Syllabus/DownloadSyllabus/' + id, { responseType: 'blob' }).subscribe(blob => {
      console.log(blob);
      saveAs(blob, 'syllabus.pdf', {
        type: 'application/pdf' // --> or whatever you need here
      });
    });
  };
  downloadTestplan(id: any) {
    this.http.get(this.baseUrl + 'api/Syllabus/DownloadtestPlan/' + id, { responseType: 'blob' }).subscribe(blob => {
      console.log(blob);
      saveAs(blob, 'testplan.pdf', {
        type: 'application/pdf' // --> or whatever you need here
      });
    });
  };
}
