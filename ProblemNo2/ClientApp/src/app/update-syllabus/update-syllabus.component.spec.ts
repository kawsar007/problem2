﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { UpdateSyllabusComponent } from './update-syllabus.component';

let component: UpdateSyllabusComponent;
let fixture: ComponentFixture<UpdateSyllabusComponent>;

describe('update-syllabus component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ UpdateSyllabusComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(UpdateSyllabusComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});