import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-update-syllabus',
    templateUrl: './update-syllabus.component.html',
    styleUrls: ['./update-syllabus.component.css']
})
/** update-syllabus component*/
export class UpdateSyllabusComponent {
    /** update-syllabus ctor */

  syllabusid: any;
  public trades: any[];
  public levels: any[];
  public http: HttpClient;
  public baseUrl: string;
  syllabus: any;
  Language: string;
  file1: File;
  file2: File;

  checkedList = [];
  xyzlist = [
    {
      id: 'En',
      value: 'English'
    },
    {
      id: 'Bn',
      value: 'Bangla'
    },
    {
      id: 'Kr',
      value: 'Korean'
    }
  ];
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private avRoute: ActivatedRoute, private router: Router) {
    this.http = http;
    this.baseUrl = baseUrl;
    this.getTrade();
    if (this.avRoute.snapshot.params['id']) {
      this.syllabusid = this.avRoute.snapshot.params['id'];
    }
  }
  ngOnInit() {
    //this.uploadForm = this.formBuilder.group({
    //  SyllabusFile: ['']
    //});

    console.log(this.syllabusid);
    if (this.syllabusid > 0) {
      this.getSyllabus(this.syllabusid);
     
    }

  }
  getSyllabus(id: any) {
    this.http.get<any[]>(this.baseUrl + 'api/Syllabus/GetSyllabus/' + id).subscribe(result => {
      this.syllabus = result;
      console.log(this.syllabus);
      this.Language = this.syllabus.language;

      this.dataChanged(this.syllabus.tradeId);
    }, error => console.error(error));
  }
  getTrade() {
    this.http.get<any[]>(this.baseUrl + 'api/Syllabus/GetTrade').subscribe(result => {
      this.trades = result;
    }, error => console.error(error));
  }

  dataChanged(newObj) {
    this.http.get<any[]>(this.baseUrl + 'api/Syllabus/GetLevel/' + newObj).subscribe(result => {
      this.levels = result;
    }, error => console.error(error));
  }


  onSubmit(f: NgForm) {
    console.log(f.value);  // { first: '', last: '' }
    console.log(f.valid);  // false
    if (f.valid) {

     
        if (this.checkedList.length > 0) {
          f.controls['Language'].setValue(this.checkedList.toString())
        } else {
          alert('At Least one Language required');
          return;
        }
      
     

      if (this.file1 == null) {
        alert('Syllabus File is required');
        return;
      }
      if (this.file2 == null) {
        alert('Test Plan File is required');
        return;
      }
      const formData = new FormData();
      formData.append('SyllabusFile', this.file1);
      formData.append('TestPlanFile', this.file2);
      formData.append('data', JSON.stringify(f.value));
      formData.append('Id', JSON.stringify(this.syllabusid));

      this.http.post<any>(this.baseUrl + 'api/Syllabus/UpdateSyllabus/', formData)
        .subscribe(
          (res) => alert('Update Successfully.'),
          (err) => console.log(err)
        );
    }
    else {
      alert('Form is invaild');
    }


  }
  onCheckboxChange(option, event) {
    if (event.target.checked) {
      this.checkedList.push(option.id);
    } else {
      for (var i = 0; i < this.xyzlist.length; i++) {
        if (this.checkedList[i] == option.id) {
          this.checkedList.splice(i, 1);
        }
      }
    }
    this.Language = this.checkedList.toString();
    //console.log(this.checkedList.toString());
  }

  Ischecked(value: any) {
    var splitted = this.syllabus.language.split(",");    
    for (var val of splitted) {
      if (val == value) {
        return true;
      }      
      //console.log(val); // prints values: 10, 20, 30, 40
    }
    return false;
  }

  onFileSelectSyllabus(event) {
    if (event.target.files.length > 0) {
      this.file1 = event.target.files[0];

    }
  }
  onFileSelectTestPlan(event) {
    if (event.target.files.length > 0) {
      this.file2 = event.target.files[0];
    }
  }
}
