﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Core.MSSQL.SqlHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProblemNo2.Models;

namespace ProblemNo2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SyllabusController : ControllerBase
    {
        public readonly ModelDbContext db;
        private readonly ISqlHelperService _sqlHelperService;
        public SyllabusController(ModelDbContext modelDbContext, ISqlHelperService sqlHelperService)
        {
            db = modelDbContext;
            _sqlHelperService = sqlHelperService;
        }
        [Route("GetSyllabus")]
        [HttpGet]
        public IActionResult GetSyllabus()
        {
            var dt = _sqlHelperService.GetDataTableBySP("GetSyllabus");
            var SyllabusList = Uitility.ConvertDataTable<Syllabusvm>(dt);
            //var TradeList = db.Database.SqlQuery<Syllabusvm>("EXEC GetSyllabus");
            return Ok(SyllabusList);
        }
        [Route("GetSyllabus/{Id}")]
        [HttpGet]
        public IActionResult GetSyllabus(int Id)
        {
            var dt = _sqlHelperService.GetDataTableBySP("GetSyllabus");
            var Syllabus = Uitility.ConvertDataTable<Syllabusvm>(dt).Where(e=>e.SyllabusId==Id).FirstOrDefault();
            //var TradeList = db.Database.SqlQuery<Syllabusvm>("EXEC GetSyllabus");
            return Ok(Syllabus);
        }
        [Route("GetTrade")]
        [HttpGet]
        public IActionResult GetTrade()
        {
            var TradeList = db.Trade.ToList();
            return Ok(TradeList);
        }
        [Route("GetLevel/{id}")]
        [HttpGet]
        public IActionResult GetLevel(int id)
        {
            var TradeLevelList = db.TradeLevel.Where(e=>e.TradeId==id).ToList();
            return Ok(TradeLevelList);
        }
        [Route("DownloadSyllabus/{id}")]
        [HttpGet]
        public IActionResult DownloadSyllabus(int id)
        {
            var syllabus = db.Syllabus.Where(e => e.SyllabusId == id).FirstOrDefault();
            var bytes = System.IO.File.ReadAllBytes(syllabus.SyllabusFile);
            return File(bytes, "application/pdf");
        }
        [Route("DownloadTestplan/{id}")]
        [HttpGet]
        public IActionResult DownloadTestplan(int id)
        {
            var syllabus = db.Syllabus.Where(e => e.SyllabusId == id).FirstOrDefault();
            var bytes = System.IO.File.ReadAllBytes(syllabus.TestplanFile);
            return File(bytes, "application/pdf");
        }

        [Route("UpdateSyllabus")]
        [HttpPost]
        public async Task<IActionResult> UpdateSyllabus()
        {
            var SyllabusFile = HttpContext.Request.Form.Files["SyllabusFile"];
            var TestPlanFile = HttpContext.Request.Form.Files["TestPlanFile"];
            var Data = HttpContext.Request.Form["data"];
           
            int Id = JsonConvert.DeserializeObject<int>(HttpContext.Request.Form["Id"]);
            CreateSyllabus createSyllabus = JsonConvert.DeserializeObject<CreateSyllabus>(Data);

            var _Syllabus = db.Syllabus.Where(e => e.SyllabusId == Id).FirstOrDefault();
            if (_Syllabus!=null)
            {
                _Syllabus.ActiveDate = createSyllabus.ActiveDate;
                _Syllabus.DevOfficer = createSyllabus.DevOfficer;
                _Syllabus.Language = createSyllabus.Language;
                _Syllabus.LevelId = createSyllabus.LevelId;
                _Syllabus.Manager = createSyllabus.Manager;
                _Syllabus.SyllabusName = createSyllabus.SyllabusName;
                _Syllabus.SyllabusFile = await UploadFile(SyllabusFile);
                _Syllabus.TestplanFile = await UploadFile(TestPlanFile);
                db.Syllabus.Update(_Syllabus);
                db.SaveChanges();
            }

            
           

            return Ok(new { msg = "Save Succesfully!" });
        }

        [Route("SaveSyllabus")]
        [HttpPost]
        public async Task<IActionResult> SaveSyllabus()
        {
            var SyllabusFile = HttpContext.Request.Form.Files["SyllabusFile"];
            var TestPlanFile = HttpContext.Request.Form.Files["TestPlanFile"];
            var Data = HttpContext.Request.Form["data"];
            CreateSyllabus createSyllabus = JsonConvert.DeserializeObject<CreateSyllabus>(Data);

            db.Syllabus.Add(new Syllabus
            {
                ActiveDate = createSyllabus.ActiveDate,
                DevOfficer = createSyllabus.DevOfficer,
                Language = createSyllabus.Language,
                LevelId = createSyllabus.LevelId,
                Manager = createSyllabus.Manager,
                SyllabusName = createSyllabus.SyllabusName,
                SyllabusFile = await UploadFile(SyllabusFile),
                TestplanFile = await UploadFile(TestPlanFile)


            });
            db.SaveChanges();

            return Ok(new { msg= "Save Succesfully!" });
        }

        private async Task<string> UploadFile(IFormFile ufile)
        {
            if (ufile != null && ufile.Length > 0)
            {
                var fileName = Path.GetFileName(ufile.FileName);
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\images", fileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await ufile.CopyToAsync(fileStream);
                }
                return filePath;
            }
            return string.Empty;
        }
    }
}