﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProblemNo2.Models
{
    public class CreateSyllabus
    {
        public int LevelId { get; set; }
        
        public string SyllabusName { get; set; }
        
        public string Language { get; set; }
        public string SyllabusFile { get; set; }
        public string TestplanFile { get; set; }
       
        public string DevOfficer { get; set; }
       
        public string Manager { get; set; }
      
        public DateTime ActiveDate { get; set; }
    }
}
