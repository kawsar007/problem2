﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProblemNo2.Models
{
    public class ModelDbContext:DbContext
    {
        public ModelDbContext(DbContextOptions<ModelDbContext> dbContextOptions):base(dbContextOptions)
        {

        }
        public DbSet<Trade> Trade { get; set; }
        public DbSet<TradeLevel> TradeLevel { get; set; }
        public DbSet<Syllabus> Syllabus { get; set; }

       
    }
}
