﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProblemNo2.Models
{
    public class Syllabus
    {
        [Key]
        public int SyllabusId { get; set; }
        [Required]
        public int LevelId { get; set; }
        [Required]
        public string SyllabusName { get; set; }
        [Required]
        public string Language { get; set; }
        public string SyllabusFile { get; set; }
        public string TestplanFile { get; set; }
        [Required]
        public string DevOfficer { get; set; }
        [Required]
        public string Manager { get; set; }
        [Required]
        public DateTime ActiveDate  { get; set; }
    }
}
