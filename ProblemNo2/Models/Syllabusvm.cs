﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProblemNo2.Models
{
    public class Syllabusvm
    {
        public int SyllabusId { get; set; }
        public int TradeId { get; set; }
        
        public int LevelId { get; set; }

        public string SyllabusName { get; set; }
        public string Language { get; set; }
        public string SyllabusFile { get; set; }
        public string TestplanFile { get; set; }
        public string DevOfficer { get; set; }
        public string Manager { get; set; }

        public DateTime ActiveDate { get; set; }
        public string TradeName { get; set; }

        public string Level { get; set; }
        public string sylfile_name { get; set; }
        public string testfile_name { get; set; }

    }
}
