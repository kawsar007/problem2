﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProblemNo2.Models
{
    public class Trade
    {
        [Key]
        public int TradeId { get; set; }
        public string TradeName { get; set; }
    }
}
