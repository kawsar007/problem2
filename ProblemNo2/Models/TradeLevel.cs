﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProblemNo2.Models
{
    public class TradeLevel
    {
        [Key]
        public int TradeLevelId { get; set; }
        public int TradeId { get; set; }
        public string Level { get; set; }
    }
}
